# PWC Security Module

![Shippable Build Status](https://img.shields.io/shippable/568d2c961895ca447467a969.svg)

## Installation

```sh
$ npm install git+ssh://git@bitbucket.org/paywithcapture/pwc_security_module.git --save
```

*Require the module in your app*

```javascript
// Set your userid
var userid = 'foo@taa.com';

// Set your passphrase
var passphrase = 'a very log string containing random characters';

var securityModule = require('pwc_security_module');
```

*For PGP Encryption*

```
// Initialize the module
var encrypt = securityModule(userid,passphrase);
```

*For AES 256 Encryption*

```
// Initialize the module
var encrypt = securityModule(userid,passphrase,'aes');
```

## Usage

> Generate Key Pairs

```javascript
encrypt.generateKeyPair()
.then(function(keys){
// keys will contain two properties; your generated privateKey and publicKey
},function(error){
// catch errors here
});
```

> Encrypt Data

```javascript
encrypt.encryptData(publicKey,data)
.then(function(encryptedData){
// get the encrypted data here
},function(error){
// catch errors here
});
```

> Decrypt Data

```javascript
encrypt.decryptData(privateKey,encryptedData)
.then(function(decryptedData){
// get the decrypted data here
},function(error){
// catch errors here
});
```

## Running unit tests

```sh
$ npm test
```

*To get the code coverage report, run this*

```sh
$ npm run coverage
```
